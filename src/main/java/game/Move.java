package game;

public class Move {
    private Position from, to;

    public Move(Position from, Position to) {
        this.from = from;
        this.to = to;
    }

    public Position getFrom() {
        return from;
    }

    public Position getSquare(int x, int y) {
        char a,b;
        a = (char)(from.getX() + x + 97);
        b = (char)(from.getY() + y + 49);
        Position p = new Position("" + a + b);
        return p;
    }

    public Position getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "Move{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
