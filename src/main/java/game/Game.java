package game;

import moveValidators.*;
import players.Player;

import java.util.Scanner;

public class Game {

    private Board chessBoard = new Board();
    private Position from; // = new Position("A2");
    private Position to; // = new Position("A4");
    private Move move = new Move(from, to);
    private MoveValidator mv;
    private Piece srcPiece;
    private Scanner scanner = new Scanner(System.in);
    private Player[] tabPlayer = new Player[2];
    private Player player;
    private boolean correctMove = true;
    private String moveString = new String();
    private String moveStringAfterRegex = new String();
    private char [] moveStringToChars;
    private String infoMessageToDisplay = "";

    public Game() {
        tabPlayer[0] = new Player(Color.WHITE); // player1
        tabPlayer[1] = new Player(Color.BLACK); // player2
    }

    private boolean ifCoordinatesAreCorrect(String moveString) {
        correctMove = false;
        if("".equals(moveString)){
            infoMessageToDisplay = "You haven't moved";
            return false;
        }
        if(!moveString.matches("[a-hA-H][1-8][ ]?[a-hA-H][1-8]")){
            return false;
        }

        moveStringToChars = moveString.toCharArray();
        if(moveStringToChars[0] == moveStringToChars[moveStringToChars.length-2] && moveStringToChars[1] == moveStringToChars[moveStringToChars.length-1]){
            return false;
        }
        if(moveStringToChars[0] >= 'A' && moveStringToChars[0] <= 'H'){
            moveStringToChars[0] = (char) (moveStringToChars[0] + 32);
        }
        if(moveStringToChars[moveStringToChars.length-2] >= 'A' && moveStringToChars[moveStringToChars.length-2] <= 'H'){
            moveStringToChars[moveStringToChars.length-2] = (char) (moveStringToChars[moveStringToChars.length-2] + 32);
        }
        moveStringAfterRegex = "" + moveStringToChars[0] + moveStringToChars[1] + " " + moveStringToChars[moveStringToChars.length-2] + moveStringToChars[moveStringToChars.length-1];
        from = new Position("" + moveStringToChars[0] + moveStringToChars[1]);
        to = new Position("" + moveStringToChars[moveStringToChars.length-2] + moveStringToChars[moveStringToChars.length-1]);

        if(chessBoard.getPieceOnPosition(from) == null){
            infoMessageToDisplay = "There is no piece on " + from + " square";
            return false;
        }

        if(!player.getColor().equals(chessBoard.getPieceOnPosition(from).getColor())){
            infoMessageToDisplay = "Your color is " + player.getColor();
            return false;
        }
        return true;
    }

    public void play() {
        clearConsole();
        insertingPlayersName();
        clearConsole();
        for(int i=0; i < tabPlayer.length; i++){
            System.out.println("Player " + (i + 1) + " " + tabPlayer[i].getName() + " (" + tabPlayer[i].getColor() + ") all moves (" + tabPlayer[i].getAllPlayerMoves().size() + "): " + tabPlayer[i].getAllPlayerMoves());
        }
        chessBoard.displayBoard();

        for(;;){
            if(chessBoard.getAllPlayersMovesCounter() % 2 == 0){
                player = tabPlayer[0];
            } else {
                player = tabPlayer[1];
            }
            if(correctMove == false){
                System.out.println("You have typed incorrect move!");
                if(!"".equals(infoMessageToDisplay)){
                    System.out.println(infoMessageToDisplay);
                    infoMessageToDisplay = "";
                }
            } else {
                if(chessBoard.if50MoveWithoutPawnsOrCaptures() ){
                    System.out.println("50 moves without pawns or captures. Draw. ");
                    System.exit(0);
                }
                if(chessBoard.ifOnlyKingsRemainWithBishopsOrKnights() ) {
                    System.out.println("Only Kings remain with Bishops or Knights. Draw.");
                    System.exit(0);
                }
                if(chessBoard.isThreefoldRepetition()) {
                    System.out.println("Threefold repetition occured. Draw.");
                    System.exit(0);
                }
            }

            // if check from opponent
            if(chessBoard.ifCheck(player.getColor() ) ){
                System.out.print("Check");
                if(chessBoard.ifCheckmate(player.getColor()) ){
                    System.out.print("mate! ");// + player.getName() + " won.");
                    System.exit(0);
                }
                System.out.println("!");
            } else {
                if(chessBoard.ifStalemate(player.getColor()) ){
                    System.out.println("Stalemate!");
                    System.exit(0);
                }
            }

            System.out.println(player.getName() + " (" + player.getColor() + ") your move: ");
            correctMove = false;
            moveString = scanner.nextLine();

            if(ifCoordinatesAreCorrect(moveString)){
                move = new Move(from, to);
                srcPiece = chessBoard.getPieceOnPosition(move.getFrom());

                switch(srcPiece.getType()){
                    case PAWN : mv = new PawnMoveValidator(); break;
                    case ROOK : mv = new RookMoveValidator(); break;
                    case KNIGHT : mv = new KnightMoveValidator(); break;
                    case BISHOP : mv = new BishopMoveValidator(); break;
                    case QUEEN : mv = new QueenMoveValidator(); break;
                    case KING : mv = new KingMoveValidator(); break;
                    default: throw new RuntimeException("No validation found for " + srcPiece.getType());
                }

                if(mv.isValid(chessBoard, move)){

                    Board boardToTest = chessBoard.copyObjectBoard(chessBoard);
                    setDoubleMove(boardToTest);
                    boardToTest.perfomMove(move);
                    // if check, if I would be in check
                    if(boardToTest.ifCheck(player.getColor())) {
                        infoMessageToDisplay = "The King would be in check.";
                        correctMove = false; // delete redundant ?
                    } else
                        {
                        setDoubleMove(chessBoard);

                        if(chessBoard.getPieceOnPosition(to) != null){
                            player.addCapturedPiece(chessBoard.getPieceOnPosition(to));
                        }

                        chessBoard.perfomMove(move);
                        correctMove = true;
                        chessBoard.setAllPlayersMovesCounter(chessBoard.getAllPlayersMovesCounter() + 1);
                        player.getAllPlayerMoves().add(moveStringAfterRegex);

                        // Pawn promotion
                        if(Type.PAWN == chessBoard.getPieceOnPosition(to).getType() && (to.getY() == 7 || to.getY() == 0) ){
                            Color pawnColor = chessBoard.getPieceOnPosition(to).getColor();
                            chessBoard.pawnPromotion(to, pawnColor);
                        }
                    }
                } else {
                    correctMove = false;
                    System.out.println("Invalid move, try again.");
                }
            }

            clearConsole();
            System.out.println("--------------------------------------------------------");

            for(int i=0; i < tabPlayer.length; i++){
                System.out.println("Player " + (i + 1) + " " + tabPlayer[i].getName() + " (" + tabPlayer[i].getColor() + "), "+ tabPlayer[i].getCapturedPieces() + ", all moves (" + tabPlayer[i].getAllPlayerMoves().size() + "): " + tabPlayer[i].getAllPlayerMoves());
            }
            chessBoard.displayBoard();
        }
    }

    private void insertingPlayersName() {
        for(int i=0; i < tabPlayer.length; i++){
            String name = "";
            while("".equals(name)){
                System.out.print("Type player "  + (i + 1) + " (" +  tabPlayer[i].getColor() + "s) name: ");
                name = scanner.nextLine();
                if("".equals(name)){
                    System.out.println("Type correct name");
                } else {
                    tabPlayer[i].setName(name);
                }
            }
        }
    }

    private void clearConsole() {
        System.out.print("\033[H\033[2J");
    }

    public void setDoubleMove(Board chessBoard) {
        if(Type.PAWN == chessBoard.getPieceOnPosition(from).getType()){
            if(Math.abs(to.getY() - from.getY()) == 2){
                chessBoard.setDoubleMove(to, chessBoard.getPieceOnPosition(from).getColor());
            } else {
                chessBoard.resetDoubleMove(chessBoard.getPieceOnPosition(from).getColor());
            }
        }
    }
}
