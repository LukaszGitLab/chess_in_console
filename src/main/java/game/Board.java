package game;

import moveValidators.*;
import java.util.*;

public class Board {

    private Piece[][] tabOfPiecesOnBoard = new Piece[8][8];
    private char blackSquare = (char) (9617);
    private char whiteSquare = (char) (9618);
    private DoubleMove doubleMoveWhite = new DoubleMove();
    private DoubleMove doubleMoveBlack = new DoubleMove();
    private Scanner scanner = new Scanner(System.in);
    private boolean firstMoveKingWhite = false;
    private boolean firstMoveKingBlack = false;
    private boolean firstMoveRookWhiteA = false;
    private boolean firstMoveRookWhiteH = false;
    private boolean firstMoveRookBlackA = false;
    private boolean firstMoveRookBlackH = false;
    private MoveValidator mv;
    private int counter50Moves = 0;
    private Map<Integer, Integer> mapForThreefoldRepetition = new HashMap<>();
    private List<Piece[][]> listForTabsOfPieces = new ArrayList<>();
    private List<Board> listForBoards = new ArrayList<>();
    private int allPlayersMovesCounter = 0;

    public Board() {
        tabOfPiecesOnBoard[0][7] = new Piece(Color.BLACK, Type.ROOK);
        tabOfPiecesOnBoard[1][7] = new Piece(Color.BLACK, Type.KNIGHT);
        tabOfPiecesOnBoard[2][7] = new Piece(Color.BLACK, Type.BISHOP);
        tabOfPiecesOnBoard[3][7] = new Piece(Color.BLACK, Type.QUEEN);
        tabOfPiecesOnBoard[4][7] = new Piece(Color.BLACK, Type.KING);
        tabOfPiecesOnBoard[5][7] = new Piece(Color.BLACK, Type.BISHOP);
        tabOfPiecesOnBoard[6][7] = new Piece(Color.BLACK, Type.KNIGHT);
        tabOfPiecesOnBoard[7][7] = new Piece(Color.BLACK, Type.ROOK);
//
        for(int i=0; i < tabOfPiecesOnBoard.length; i++){
            tabOfPiecesOnBoard[i][6] = new Piece(Color.BLACK, Type.PAWN);
            tabOfPiecesOnBoard[i][1] = new Piece(Color.WHITE, Type.PAWN);
        }

        tabOfPiecesOnBoard[0][0] = new Piece(Color.WHITE, Type.ROOK);
        tabOfPiecesOnBoard[1][0] = new Piece(Color.WHITE, Type.KNIGHT);
        tabOfPiecesOnBoard[2][0] = new Piece(Color.WHITE, Type.BISHOP);
        tabOfPiecesOnBoard[3][0] = new Piece(Color.WHITE, Type.QUEEN);
        tabOfPiecesOnBoard[4][0] = new Piece(Color.WHITE, Type.KING);
        tabOfPiecesOnBoard[5][0] = new Piece(Color.WHITE, Type.BISHOP);
        tabOfPiecesOnBoard[6][0] = new Piece(Color.WHITE, Type.KNIGHT);
        tabOfPiecesOnBoard[7][0] = new Piece(Color.WHITE, Type.ROOK);
    }

    public Piece getPieceOnPosition(Position position){
        return tabOfPiecesOnBoard[position.getX()][position.getY()];
    }

    public void perfomMove(Move move) {
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();

        if(Type.PAWN == tabOfPiecesOnBoard[xFrom][yFrom].getType()
                || tabOfPiecesOnBoard[xTo][yTo] != null ){
            counter50Moves = 0;
        } else {
            counter50Moves++;
        }

        tabOfPiecesOnBoard[xTo][yTo] = tabOfPiecesOnBoard[xFrom][yFrom];
        tabOfPiecesOnBoard[xFrom][yFrom] = null;

        if(Type.KING == tabOfPiecesOnBoard[xTo][yTo].getType() ){
            if(Color.WHITE == tabOfPiecesOnBoard[xTo][yTo].getColor() ) {
                if(isFirstMoveKingWhite() == false){
                    setFirstMoveKingWhite(true);
                }
                // whites rook castling
                if(Math.abs(xFrom - xTo) == 2) {
                    rookMoveInCastling(xFrom - xTo, 0);
                }
            } else {
                if(isFirstMoveKingBlack() == false){
                    setFirstMoveKingBlack(true);
                }
                // blacks rook castling
                if(Math.abs(xFrom - xTo) == 2){
                    rookMoveInCastling(xFrom - xTo, 7);
                }
            }
        }
    }

    public void rookMoveInCastling(int value, int column){
        if(value == 2){
            tabOfPiecesOnBoard[3][column] = tabOfPiecesOnBoard[0][column];
            tabOfPiecesOnBoard[0][column] = null;
        } else if(value == -2){
            tabOfPiecesOnBoard[5][column] = tabOfPiecesOnBoard[7][column];
            tabOfPiecesOnBoard[7][column] = null;
        }
    }

    public void displayBoard(){
        int counter = 0;
        System.out.println("\n     a b c d e f g h\n");
            for(int i = tabOfPiecesOnBoard.length-1; i >= 0 ; i--){
            System.out.print((i + 1) + "   ");
            for(int j = 0; j < tabOfPiecesOnBoard.length; j++){
                if(tabOfPiecesOnBoard[j][i] != null){
                    if(tabOfPiecesOnBoard[j][i].getColor() == Color.WHITE){
                        switch(tabOfPiecesOnBoard[j][i].getType()){
                            case KING: System.out.print("|" + (char)(9812)); break;
                            case QUEEN: System.out.print("|" + (char)(9813)); break;
                            case ROOK: System.out.print("|" + (char)(9814)); break;
                            case BISHOP: System.out.print("|" + (char)(9815)); break;
                            case KNIGHT: System.out.print("|" + (char)(9816)); break;
                            case PAWN: System.out.print("|" + (char)(9817)); break;
                            default: System.out.print("Wrong Piece");
                        }
                    } else if(tabOfPiecesOnBoard[j][i].getColor() == Color.BLACK){
                        switch(tabOfPiecesOnBoard[j][i].getType()){
                            case KING: System.out.print("|" + (char)(9818)); break;
                            case QUEEN: System.out.print("|" + (char)(9819)); break;
                            case ROOK: System.out.print("|" + (char)(9820)); break;
                            case BISHOP: System.out.print("|" + (char)(9821)); break;
                            case KNIGHT: System.out.print("|" + (char)(9822)); break;
                            case PAWN: System.out.print("|" + (char)(9823)); break;
                            default: System.out.print("Wrong Piece");
                        }
                    }
                } else {
                    if(counter % 2 == 0){
                        System.out.print("|" + whiteSquare);
                    } else {
                        System.out.print("|" + blackSquare);
                    }
                }
                counter++;
            }
            counter--;
            System.out.println("|   " + (i + 1));
        }
        System.out.println("\n     a b c d e f g h\n");
    }

    public void setDoubleMove(Position position, Color color) {
        if(color == Color.WHITE){
            doubleMoveWhite = new DoubleMove(position);
        } else {
            doubleMoveBlack = new DoubleMove(position);
        }
    }

    public void resetDoubleMove(Color color) {
        if(color == Color.WHITE){
            doubleMoveWhite = new DoubleMove();
        } else {
            doubleMoveBlack = new DoubleMove();
        }
    }

    public Position getDoubleMovePosition(Color oppositeColor) {
//        try {
            if(Color.WHITE != oppositeColor){
                return doubleMoveBlack.getPosition();
            } else {
                return doubleMoveWhite.getPosition();
            }
    }

    public void pawnPromotion(Position to, Color colorOfPawn){
        String valuePromotion = "";
        System.out.println("Pawn promotion. Promote pawn to: 1 - Queen, 2 - Rook, 3 - Knight, 4 - Bishop");
        while(!valuePromotion.matches("[1234]") ) {
            valuePromotion = scanner.nextLine();
            if(valuePromotion.matches("[1234]")) {
                break;
            }
            System.out.println("You have chosen wrong value, select from 1 to 4");
        }
        int x = to.getX();
        int y = to.getY();
        switch(valuePromotion){
            case "1" : tabOfPiecesOnBoard[x][y] = new Piece(colorOfPawn, Type.QUEEN); break;
            case "2" : tabOfPiecesOnBoard[x][y] = new Piece(colorOfPawn, Type.ROOK); break;
            case "3" : tabOfPiecesOnBoard[x][y] = new Piece(colorOfPawn, Type.KNIGHT); break;
            case "4" : tabOfPiecesOnBoard[x][y] = new Piece(colorOfPawn, Type.BISHOP); break;
        }
    }

    public boolean ifCheck(Color colorKing){
        Piece pieceRoundabout;
        int xTemp, yTemp, x,y, xMultiplier = 0, yMultiplier = 0;
        xTemp = getPositionKing(colorKing).getX();
        yTemp = getPositionKing(colorKing).getY();

        // dla wszystkich kierunków, bez KNIGHT
        for(int j=0; j < 8; j++){
            x = xTemp;
            y = yTemp;
            switch (j){
                case 0: xMultiplier = 1; yMultiplier = 0; break;
                case 1: xMultiplier = -1; yMultiplier = 0; break;
                case 2: xMultiplier = 0; yMultiplier = 1; break;
                case 3: xMultiplier = 0; yMultiplier = -1; break;
                case 4: xMultiplier = 1; yMultiplier = 1; break;
                case 5: xMultiplier = 1; yMultiplier = -1; break;
                case 6: xMultiplier = -1; yMultiplier = 1; break;
                case 7: xMultiplier = -1; yMultiplier = -1; break;
                default: break;
            }
            while(y <= 7 && y >= 0 && x <= 7 && x >= 0){
                x = x + 1 * xMultiplier;
                if(x > 7 || x < 0){
                    break;
                }
                y = y + 1 * yMultiplier;
                if(y > 7 || y < 0){
                    break;
                }
                if(tabOfPiecesOnBoard[x][y] != null ) {
                    pieceRoundabout = tabOfPiecesOnBoard[x][y];
                    if(colorKing != pieceRoundabout.getColor()){
                        if(Type.QUEEN == pieceRoundabout.getType() ){
                            return true;
                        } else if(xMultiplier * yMultiplier == 0 && Type.ROOK == pieceRoundabout.getType()) {
                            return true;
                        } else if(xMultiplier * yMultiplier != 0 && Type.BISHOP == pieceRoundabout.getType()) {
                            return true;
                        } else if((Math.abs(x - xTemp) == 1 || Math.abs(y - yTemp) == 1)
                                && Type.KING == pieceRoundabout.getType() ){
                            return true;
                        } else if( ((x - xTemp) == 1 || (x - xTemp) == -1 ) && (y - yTemp) == 1
                                && colorKing == Color.WHITE
                                && Type.PAWN == pieceRoundabout.getType() ){
                            return true;
                        } else if( ((x - xTemp) == 1 || (x - xTemp) == -1 ) && (y - yTemp) == -1
                                && colorKing == Color.BLACK
                                && Type.PAWN == pieceRoundabout.getType() ){
                            return true;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        //for KNIGHT
        for(int i=0; i < 8; i++){
            x = xTemp;
            y = yTemp;
            switch (i){
                case 0: x = x + 1; y = y + 2; break;
                case 1: x = x + 2; y = y + 1; break;
                case 2: x = x + 1; y = y - 2; break;
                case 3: x = x + 2; y = y - 1; break;
                case 4: x = x - 1; y = y + 2; break;
                case 5: x = x - 2; y = y + 1; break;
                case 6: x = x - 1; y = y - 2; break;
                case 7: x = x - 2; y = y - 1; break;
                default: break;
            }

            if(x <= 7 && x >= 0 && y <= 7 && y >= 0){
                if(tabOfPiecesOnBoard[x][y] != null){
                    pieceRoundabout = tabOfPiecesOnBoard[x][y];
                    if(colorKing != pieceRoundabout.getColor()
                            && Type.KNIGHT == pieceRoundabout.getType() ){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean ifCheckmate(Color colorKing){
        Piece srcPiece;
        int x,y, xMultiplier = 0, yMultiplier = 0;
        Board board;
        Position from = new Position();
        Position to = new Position();

        for(int iY = tabOfPiecesOnBoard.length-1; iY >= 0 ; iY--){
            for(int jX = 0; jX < tabOfPiecesOnBoard.length; jX++){
                if(tabOfPiecesOnBoard[jX][iY] != null && tabOfPiecesOnBoard[jX][iY].getColor() == colorKing ) {
                    srcPiece = tabOfPiecesOnBoard[jX][iY];
                    switch(srcPiece.getType()){
                        case PAWN : mv = new PawnMoveValidator(); break;
                        case ROOK : mv = new RookMoveValidator(); break;
                        case KNIGHT : mv = new KnightMoveValidator(); break;
                        case BISHOP : mv = new BishopMoveValidator(); break;
                        case QUEEN : mv = new QueenMoveValidator(); break;
                        case KING : mv = new KingMoveValidator(); break;
                        default: throw new RuntimeException("No validation found for " + tabOfPiecesOnBoard[jX][iY].getType() + " in ifCheckmate");
                    }

                    for(int k=0; k<8; k++){
                        board = this.copyObjectBoard(this);
                        x = jX;
                        y = iY;
                        if(k < 4 &&
                                (srcPiece.getType() == Type.KING
                                || srcPiece.getType() == Type.QUEEN
                                || srcPiece.getType() == Type.ROOK ) ) {
                            switch (k){
                                case 0: xMultiplier = 1; yMultiplier = 0; break;
                                case 1: xMultiplier = -1; yMultiplier = 0; break;
                                case 2: xMultiplier = 0; yMultiplier = 1; break;
                                case 3: xMultiplier = 0; yMultiplier = -1; break;
                            }
                        } else if( // k > 3 &&
                                (srcPiece.getType() == Type.KING
                                || srcPiece.getType() == Type.QUEEN
                                || srcPiece.getType() == Type.BISHOP ) ) {
                            switch (k){
                                case 4: xMultiplier = 1; yMultiplier = 1; break;
                                case 5: xMultiplier = 1; yMultiplier = -1; break;
                                case 6: xMultiplier = -1; yMultiplier = 1; break;
                                case 7: xMultiplier = -1; yMultiplier = -1; break;
                            }
                        } else if(srcPiece.getType() == Type.PAWN && Color.WHITE == colorKing ){
                            switch (k){
                                case 0: xMultiplier = 0; yMultiplier = 1; break;
                                case 1: xMultiplier = 1; yMultiplier = 1; break;
                                case 2: xMultiplier = -1; yMultiplier = 1; break;
                                case 3: xMultiplier = 0; yMultiplier = 2; break;
                            }
                        } else if(srcPiece.getType() == Type.PAWN && Color.BLACK == colorKing) {
                            switch (k){
                                case 0: xMultiplier = 0; yMultiplier = -1; break;
                                case 1: xMultiplier = 1; yMultiplier = -1; break;
                                case 2: xMultiplier = -1; yMultiplier = -1; break;
                                case 3: xMultiplier = 0; yMultiplier = -2; break;
                            }
                        } else if(srcPiece.getType() == Type.KNIGHT) {
                            from.setX(x);
                            from.setY(y);
                            switch (k){
                                case 0: x = x + 1; y = y + 2; break;
                                case 1: x = x + 2; y = y + 1; break;
                                case 2: x = x + 1; y = y - 2; break;
                                case 3: x = x + 2; y = y - 1; break;
                                case 4: x = x - 1; y = y + 2; break;
                                case 5: x = x - 2; y = y + 1; break;
                                case 6: x = x - 1; y = y - 2; break;
                                case 7: x = x - 2; y = y - 1; break;
                                default: throw new RuntimeException("Wrong case number");
                            }
                        } else {
                            break;
                        }

                        while(y <= 7 && y >= 0 && x <= 7 && x >= 0){
                            if(srcPiece.getType() != Type.KNIGHT) {
                                from.setX(x);
                                from.setY(y);
                                x = x + 1 * xMultiplier;
                                if(x > 7 || x < 0){
                                    break;
                                }
                                y = y + 1 * yMultiplier;
                                if(y > 7 || y < 0){
                                    break;
                                }
                            }

                            to.setX(x);
                            to.setY(y);

                            Move moveTest = new Move(from, to);
                            if(!mv.isValid(board, moveTest)) {
                                break;
                            }
                            board.perfomMove(moveTest);
                            if(board.ifCheck(colorKing) == false ) {
//                                System.out.println("$   Is possible move to avoid check if move make\n$   " + board.getPieceOnPosition(to).getType()
//                                        + " from Position[x=" + jX + ", y=" + iY + "]"
//                                        + " to " + to);
                                return false;
                            }
                            if(srcPiece.getType() == Type.PAWN || srcPiece.getType() == Type.KING || srcPiece.getType() == Type.KNIGHT) {
                                break; // aby był tylko jeden ruch w jedną stronę
                            }
                        }
                    } // koniec for int k
                } // koniec if
            }
        }

        return true; // true mean checkmate!
    }

    public boolean if50MoveWithoutPawnsOrCaptures(){
        if(counter50Moves == 50){
           return true;
        }
        return false;
    }

    public boolean ifStalemate(Color colorKing){
        if(ifCheckmate(colorKing)) {
            return true;
        }
        return false;
    }

    public Position getPositionKing(Color colorKing) {
        Position positionKing = new Position();
        for(int i = tabOfPiecesOnBoard.length-1; i >= 0 ; i--){
            for(int j = 0; j < tabOfPiecesOnBoard.length; j++) {
                if(tabOfPiecesOnBoard[j][i] != null) {
                    if (tabOfPiecesOnBoard[j][i].getType() == Type.KING
                            && tabOfPiecesOnBoard[j][i].getColor() == colorKing) {
                        positionKing.setX(j);
                        positionKing.setY(i);
                        break;
                    }
                }
            }
        }
        return positionKing;
    }

    public Board copyObjectBoard(Board boardToClone) {
        Board boardToReturn = new Board();
        Piece[][] tabBoard = copy8x8Tab(boardToClone.getTabOfPiecesOnBoard());
        boardToReturn.setTabOfPiecesOnBoard(tabBoard);

        if(boardToClone.getDoubleMovePosition(Color.WHITE).getX() != -1){
            Position positionDoubleWhite = new Position();
            positionDoubleWhite.setX(boardToClone.getDoubleMovePosition(Color.WHITE).getX());
            positionDoubleWhite.setY(boardToClone.getDoubleMovePosition(Color.WHITE).getY());
            boardToReturn.setDoubleMove(positionDoubleWhite, Color.WHITE);
        } else {
            boardToReturn.resetDoubleMove(Color.WHITE);
        }

        if(boardToClone.getDoubleMovePosition(Color.BLACK).getX() != -1){
            Position positionDoubleBlack = new Position();
            positionDoubleBlack.setX(boardToClone.getDoubleMovePosition(Color.BLACK).getX());
            positionDoubleBlack.setY(boardToClone.getDoubleMovePosition(Color.BLACK).getY());
            boardToReturn.setDoubleMove(positionDoubleBlack, Color.BLACK);
        } else {
            boardToReturn.resetDoubleMove(Color.BLACK);
        }

        boardToReturn.setFirstMoveKingWhite(boardToClone.isFirstMoveKingWhite());
        boardToReturn.setFirstMoveKingBlack(boardToClone.isFirstMoveKingBlack());
        boardToReturn.setFirstMoveRookWhiteA(boardToClone.isFirstMoveRookWhiteA());
        boardToReturn.setFirstMoveRookWhiteH(boardToClone.isFirstMoveRookWhiteH());
        boardToReturn.setFirstMoveRookBlackA(boardToClone.isFirstMoveRookBlackA());
        boardToReturn.setFirstMoveRookBlackH(boardToClone.isFirstMoveRookBlackH());
        boardToReturn.setCounter50Moves(boardToClone.getCounter50Moves());

        boardToReturn.setAllPlayersMovesCounter(boardToClone.getAllPlayersMovesCounter());

        mapForThreefoldRepetition = new HashMap<>(boardToClone.getMapForThreefoldRepetition());
        listForTabsOfPieces = new ArrayList<>(boardToClone.getListForTabsOfPieces());
        listForBoards = new ArrayList<>(boardToClone.getListForBoards());

        return boardToReturn;
    }

    public boolean isThreefoldRepetition() {
        Piece[][] currentTab = copy8x8Tab(getTabOfPiecesOnBoard());
        Piece[][] tabTemp;
        int temp;

        if(listForTabsOfPieces.isEmpty()){
            listForTabsOfPieces.add(currentTab);
            mapForThreefoldRepetition.put(0,1);
            Board tempBoard = copyObjectBoard(this);
            listForBoards.add(tempBoard);
            return false;
        }

        for(int  i=0; i < listForTabsOfPieces.size(); i++) {
            tabTemp = copy8x8Tab(listForTabsOfPieces.get(i));
            TwoLoops:
            for(int w=0; w < tabTemp.length; w++){
                for(int z=0; z < tabTemp.length; z++){
                    if((tabTemp[w][z] != null && currentTab[w][z] == null) || (tabTemp[w][z] == null && currentTab[w][z] != null)) {
                        break TwoLoops;
                    }
                }

                if(w == tabTemp.length - 1 ) {
                    if( ( (this.getAllPlayersMovesCounter() % 2 == 0) == (listForBoards.get(i).getAllPlayersMovesCounter() % 2 == 0) )
                        && ((this.isPossibleCastlingQueenSideWhite() ) == (listForBoards.get(i).isPossibleCastlingQueenSideWhite() ) )
                        && ((this.isPossibleCastlingQueenSideBlack() ) == (listForBoards.get(i).isPossibleCastlingQueenSideBlack() ) )
                        && ((this.isPossibleCastlingKingSideWhite() ) == (listForBoards.get(i).isPossibleCastlingKingSideWhite() ) )
                        && ((this.isPossibleCastlingKingSideBlack() ) == (listForBoards.get(i).isPossibleCastlingKingSideBlack() ) )
                        && (this.isPossibleCaptureEnPassant(Color.WHITE) == listForBoards.get(i).isPossibleCaptureEnPassant(Color.WHITE) )
                        && (this.isPossibleCaptureEnPassant(Color.BLACK) == listForBoards.get(i).isPossibleCaptureEnPassant(Color.BLACK) )
                    ) {

                        temp = mapForThreefoldRepetition.get(i);
                        temp = temp + 1;
                        if (temp == 3) {
                            mapForThreefoldRepetition.put(i, temp);
                            listForBoards.get(i).getMapForThreefoldRepetition().put(i, temp);
                            System.out.println(mapForThreefoldRepetition);
                            return true;
                        }

                        mapForThreefoldRepetition.put(i, temp);
                        listForBoards.get(i).getMapForThreefoldRepetition().put(i, temp);
                        System.out.println(mapForThreefoldRepetition);
                        return false;
                    }
                }
            } // end TwoLoops
        }

        mapForThreefoldRepetition.put(listForTabsOfPieces.size(),1);
        listForTabsOfPieces.add(currentTab);
        Board tempBoard = copyObjectBoard(this);
        listForBoards.add(tempBoard);
        return false;
    }



    public boolean isPossibleCaptureEnPassant(Color color){
        int xDouble;
        int yDouble;
        Piece piece;

        if(getDoubleMovePosition(color).getX() != -1) {
            xDouble = getDoubleMovePosition(color).getX();
            yDouble = getDoubleMovePosition(color).getY();

            if(xDouble > 0 && xDouble < 7) {
                piece = getPieceOnPosition(new Position(xDouble + 1, yDouble));
                if(piece == null) {
                    piece = getPieceOnPosition(new Position(xDouble - 1, yDouble));
                }
                if(piece == null) {
                    return false;
                }
            } else if(xDouble == 0) {
                piece = getPieceOnPosition(new Position(xDouble + 1, yDouble));
                if(piece == null) {
                    return false;
                }
            } else if(xDouble == 7) {
                piece = getPieceOnPosition(new Position(xDouble - 1, yDouble));
                if(piece == null) {
                    return false;
                }
            } else {
                return false;
            }

            if(piece.getType() == Type.PAWN && piece.getColor() != color) {
                return true;
            }
        }

        return false;
    }

    public Piece[][] copy8x8Tab(Piece[][] tabToCopy) {
        Piece[][] tabToReturn = new Piece[8][8];
        for(int w=0; w < 8; w++){
            for(int z=0; z < 8; z++){
                tabToReturn[w][z] = tabToCopy[w][z];
            }
        }
        return tabToReturn;
    }

    public boolean ifOnlyKingsRemainWithBishopsOrKnights() {
        List<Piece> list = new ArrayList<>();
        Piece bishop1 = null;
        Piece bishop2 = null;
        Color colorSquareBishop1 = null;
        Color colorSquareBishop2 = null;

        for(int i = tabOfPiecesOnBoard.length-1; i >= 0 ; i--){
            for(int j = 0; j < tabOfPiecesOnBoard.length; j++) {
                if(tabOfPiecesOnBoard[j][i] != null) {
                    if (tabOfPiecesOnBoard[j][i].getType() == Type.KING
                            || tabOfPiecesOnBoard[j][i].getType() == Type.BISHOP
                            || tabOfPiecesOnBoard[j][i].getType() == Type.KNIGHT ) {
                        if(tabOfPiecesOnBoard[j][i].getType() == Type.BISHOP) {
                            if(((j%2) == 0 && (i%2) == 0) || ((j%2) != 0 && (i%2) != 0) ){
                                if(colorSquareBishop1 == null) {
                                    colorSquareBishop1 = Color.BLACK;
                                } else if(colorSquareBishop2 == null) {
                                    colorSquareBishop2 = Color.BLACK;
                                } else {
                                    return false;
                                }
                            } else {
                                if(colorSquareBishop1 == null) {
                                    colorSquareBishop1 = Color.WHITE;
                                } else if(colorSquareBishop2 == null) {
                                    colorSquareBishop2 = Color.WHITE;
                                } else {
                                    return false;
                                }
                            }
                        }
                        list.add(tabOfPiecesOnBoard[j][i]);
                    } else {
                        return false;
                    }
                }
            }
        }

        if(list.size() <= 3) {
            return true;
        }
        if(list.size() == 4 ) {
            for(Piece piece : list) {
                if(piece.getType() == Type.BISHOP ){
                    if(bishop1 == null) {
                        bishop1 = new Piece(piece.getColor(), piece.getType());
                    } else  {
                        bishop2 = new Piece(piece.getColor(), piece.getType());
                        break;
                    }
                }
            }
            if(bishop1 != null && bishop2 != null) {
                if(bishop1.getColor() == bishop2.getColor()) {
                    return false;
                }
                if(bishop1.getColor() != bishop2.getColor() && colorSquareBishop1 == colorSquareBishop2 ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isPossibleCastlingQueenSideWhite() {
        if(isFirstMoveKingWhite() == true || isFirstMoveRookWhiteA() == true ){
            return false;
        }
        return true;
    }

    public boolean isPossibleCastlingQueenSideBlack() {
        if(isFirstMoveKingBlack() == true || isFirstMoveRookBlackA() == true ){
            return false;
        }
        return true;
    }

    public boolean isPossibleCastlingKingSideWhite() {
        if(isFirstMoveKingWhite() == true || isFirstMoveRookWhiteH() == true ){
            return false;
        }
        return true;
    }

    public boolean isPossibleCastlingKingSideBlack() {
        if(isFirstMoveKingBlack() == true || isFirstMoveRookBlackH() == true ){
            return false;
        }
        return true;
    }

    public Piece[][] getTabOfPiecesOnBoard() {
        return tabOfPiecesOnBoard;
    }

    public void setTabOfPiecesOnBoard(Piece[][] tabOfPiecesOnBoard) {
        this.tabOfPiecesOnBoard = tabOfPiecesOnBoard;
    }

    public boolean isFirstMoveKingWhite() {
        return firstMoveKingWhite;
    }

    public void setFirstMoveKingWhite(boolean firstMoveKingWhite) {
        this.firstMoveKingWhite = firstMoveKingWhite;
    }

    public boolean isFirstMoveKingBlack() {
        return firstMoveKingBlack;
    }

    public void setFirstMoveKingBlack(boolean firstMoveKingBlack) {
        this.firstMoveKingBlack = firstMoveKingBlack;
    }

    public boolean isFirstMoveRookWhiteA() {
        return firstMoveRookWhiteA;
    }

    public void setFirstMoveRookWhiteA(boolean firstMoveRookWhiteA) {
        this.firstMoveRookWhiteA = firstMoveRookWhiteA;
    }

    public boolean isFirstMoveRookWhiteH() {
        return firstMoveRookWhiteH;
    }

    public void setFirstMoveRookWhiteH(boolean firstMoveRookWhiteH) {
        this.firstMoveRookWhiteH = firstMoveRookWhiteH;
    }

    public boolean isFirstMoveRookBlackA() {
        return firstMoveRookBlackA;
    }

    public void setFirstMoveRookBlackA(boolean firstMoveRookBlackA) {
        this.firstMoveRookBlackA = firstMoveRookBlackA;
    }

    public boolean isFirstMoveRookBlackH() {
        return firstMoveRookBlackH;
    }

    public void setFirstMoveRookBlackH(boolean firstMoveRookBlackH) {
        this.firstMoveRookBlackH = firstMoveRookBlackH;
    }

    public int getCounter50Moves() {
        return counter50Moves;
    }

    public void setCounter50Moves(int counter50Moves) {
        this.counter50Moves = counter50Moves;
    }

    public int getAllPlayersMovesCounter() {
        return allPlayersMovesCounter;
    }

    public void setAllPlayersMovesCounter(int allPlayersMovesCounter) {
        this.allPlayersMovesCounter = allPlayersMovesCounter;
    }

    public Map<Integer, Integer> getMapForThreefoldRepetition() {
        return mapForThreefoldRepetition;
    }

    public List<Piece[][]> getListForTabsOfPieces() {
        return listForTabsOfPieces;
    }

    public List<Board> getListForBoards() {
        return listForBoards;
    }
}
