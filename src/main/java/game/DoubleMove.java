package game;

public class DoubleMove {

    private Position position;

    public DoubleMove(Position position) {
        this.position = position;
    }

    public DoubleMove() {
        position = new Position();
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "DoubleMove{" +
                "position=" + position +
                '}';
    }
}
