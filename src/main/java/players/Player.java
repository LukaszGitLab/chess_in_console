package players;

import game.Color;
import game.Piece;
import java.util.*;

public class Player {
    private String name;
    private Color color;
    private List<String> allPlayerMoves = new ArrayList<>();
    private List<Character> capturedPieces = new ArrayList<>();

    public Player(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public List<String> getAllPlayerMoves() {
        return allPlayerMoves;
    }

    public List<Character> getCapturedPieces() {
        return capturedPieces;
    }

    public void addCapturedPiece(Piece capturedPiece) {
        if(capturedPiece.getColor() == Color.WHITE){
            switch(capturedPiece.getType()){
                case KING: this.capturedPieces.add((char)(9812)); break;
                case QUEEN: this.capturedPieces.add((char)(9813)); break;
                case ROOK: this.capturedPieces.add((char)(9814)); break;
                case BISHOP: this.capturedPieces.add((char)(9815)); break;
                case KNIGHT: this.capturedPieces.add((char)(9816)); break;
                case PAWN: this.capturedPieces.add((char)(9817)); break;
                default: System.out.print("Wrong Piece");
            }
        } else if(capturedPiece.getColor() == Color.BLACK){
            switch(capturedPiece.getType()){
                case KING: this.capturedPieces.add((char)(9818)); break;
                case QUEEN: this.capturedPieces.add((char)(9819)); break;
                case ROOK: this.capturedPieces.add((char)(9820)); break;
                case BISHOP: this.capturedPieces.add((char)(9821)); break;
                case KNIGHT: this.capturedPieces.add((char)(9822)); break;
                case PAWN: this.capturedPieces.add((char)(9823)); break;
                default: System.out.print("Wrong Piece");
            }
        }
        Collections.sort(this.capturedPieces);
    }
}
