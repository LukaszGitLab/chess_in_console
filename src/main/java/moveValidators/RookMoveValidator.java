package moveValidators;

import game.*;

public class RookMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();
        int j,k;

        if(xFrom == xTo){
            j = yTo > yFrom ? 1 : -1 ;
            k = Math.abs(yTo - yFrom);
            for(int i=1; i < k; i++){
                if(chessBoard.getPieceOnPosition(move.getSquare(0, i * j)) != null ) {
                    return false;
                }
            }
        } else if(yFrom == yTo){
            j = xTo > xFrom ? 1 : -1 ;
            k = Math.abs(xTo - xFrom);
            for(int i=1; i < k; i++){
                if(chessBoard.getPieceOnPosition(move.getSquare(i * j, 0)) != null){
                    return false;
                }
            }
        } else {
            return false;
        }

        if(chessBoard.getPieceOnPosition(move.getTo()) != null
                && (chessBoard.getPieceOnPosition(move.getFrom()).getColor() == chessBoard.getPieceOnPosition(move.getTo()).getColor()) ) {
            return false;
        }

        if(chessBoard.isFirstMoveRookWhiteA() == false
                && xFrom == 0
                && yFrom == 0 ){
            chessBoard.setFirstMoveRookWhiteA(true);
        } else if(chessBoard.isFirstMoveRookWhiteH() == false
                && xFrom == 7
                && yFrom == 0 ){
            chessBoard.setFirstMoveRookWhiteH(true);
        } else if(chessBoard.isFirstMoveRookBlackA() == false
                && xFrom == 0
                && yFrom == 7 ){
            chessBoard.setFirstMoveRookBlackA(true);
        } else if(chessBoard.isFirstMoveRookBlackH() == false
                && xFrom == 7
                && yFrom == 7 ){
            chessBoard.setFirstMoveRookBlackH(true);
        }

        return true;
    }
}
