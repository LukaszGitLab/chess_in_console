package moveValidators;

import game.*;

public class BishopMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();
        int j1, j2, k;

        if(Math.abs(xFrom - xTo) == Math.abs(yFrom - yTo) ){
            j1 = xTo > xFrom ? 1 : -1 ;
            j2 = yTo > yFrom ? 1 : -1 ;
            k = Math.abs(yTo - yFrom);
            for(int i=1; i < k; i++){
                if(chessBoard.getPieceOnPosition(move.getSquare(i * j1, i * j2)) != null ) {
                    return false;
                }
            }
        } else {
            return false;
        }

        if(chessBoard.getPieceOnPosition(move.getTo()) != null
                && chessBoard.getPieceOnPosition(move.getFrom()).getColor()
                == chessBoard.getPieceOnPosition(move.getTo()).getColor() ) {
            return false;
        }

        return true;
    }
}
