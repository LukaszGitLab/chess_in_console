package moveValidators;

import game.Board;
import game.Move;

public interface MoveValidator {
    boolean isValid(Board chessBoard, Move move);
}
