package moveValidators;

import game.*;

public class KingMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        Color pieceColor = chessBoard.getPieceOnPosition(move.getFrom()).getColor();
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();
        Board board = chessBoard.copyObjectBoard(chessBoard);

        if(Math.abs(xFrom - xTo) == 1 && Math.abs(yFrom - yTo) == 1
                || Math.abs(xFrom - xTo) == 0 && Math.abs(yFrom - yTo) == 1
                || Math.abs(xFrom - xTo) == 1 && Math.abs(yFrom - yTo) == 0 ){
            return new QueenMoveValidator().isValid(chessBoard, move);
        } else if(xFrom - xTo == 2) { // // castling Queen side
            if(Color.WHITE == pieceColor ){
                if(chessBoard.isPossibleCastlingQueenSideWhite() ){
                    return false;
                }
            } else {
                if(chessBoard.isPossibleCastlingQueenSideBlack() ){
                    return false;
                }
            }
            for(int i=1; i < 4; i++){
               if(chessBoard.getPieceOnPosition(move.getSquare(-i,0)) != null ){
                   return false;
               }
            }
            // czy pola są szachowane
            if(chessBoard.ifCheck(chessBoard.getPieceOnPosition(move.getFrom()).getColor()) ){
                return false;
            }
            // 1
            board.perfomMove(new Move(move.getFrom(), move.getSquare(-1, 0)));
            if(board.ifCheck(board.getPieceOnPosition(move.getSquare(-1, 0)).getColor()) ){
                return false;
            }
            // 2
            board.perfomMove(new Move(move.getSquare(-1, 0), move.getSquare(-2, 0)));
            if(board.ifCheck(board.getPieceOnPosition(move.getSquare(-2, 0)).getColor()) ){
                return false;
            }
            return true;
        } else if(xFrom - xTo == -2) { // castling King side
            if(Color.WHITE == pieceColor ){
                if(chessBoard.isPossibleCastlingKingSideWhite() == false){
                    return false;
                }
            } else {
                if(chessBoard.isPossibleCastlingKingSideBlack() == false){
                    return false;
                }
            }

            for(int i=1; i < 3; i++){
                if(chessBoard.getPieceOnPosition(move.getSquare(i,0)) != null ){
                    return false;
                }
            }
            // czy pola są szachowane
            if(chessBoard.ifCheck(chessBoard.getPieceOnPosition(move.getFrom()).getColor()) ){
                return false;
            }
            System.out.println();
            // 1
            board.perfomMove(new Move(move.getFrom(), move.getSquare(1, 0)));
            if(board.ifCheck(board.getPieceOnPosition(move.getSquare(1, 0)).getColor()) ){
                return false;
            }
            // 2
            board.perfomMove(new Move(move.getSquare(1, 0), move.getSquare(2, 0)));
            if(board.ifCheck(board.getPieceOnPosition(move.getSquare(2, 0)).getColor()) ){
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
