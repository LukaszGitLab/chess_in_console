package moveValidators;

import game.*;

public class KnightMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();

        if(Math.abs(xFrom - xTo) == 1 && Math.abs(yFrom - yTo) == 2
                || Math.abs(xFrom - xTo) == 2 && Math.abs(yFrom - yTo) == 1 ){

            if(chessBoard.getPieceOnPosition(move.getTo()) != null
                    && chessBoard.getPieceOnPosition(move.getFrom()).getColor() == chessBoard.getPieceOnPosition(move.getTo()).getColor() ) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
