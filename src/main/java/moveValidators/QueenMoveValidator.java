package moveValidators;

import game.Board;
import game.Move;

public class QueenMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        if(new RookMoveValidator().isValid(chessBoard, move)){
            return true;
        } else if(new BishopMoveValidator().isValid(chessBoard, move)){
            return true;
        }
        return false;
    }
}
