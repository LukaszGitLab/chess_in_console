package moveValidators;

import game.*;

public class PawnMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(Board chessBoard, Move move) {
        int xFrom = move.getFrom().getX();
        int yFrom = move.getFrom().getY();
        int xTo = move.getTo().getX();
        int yTo = move.getTo().getY();

        if(xFrom == xTo){
            if((!wasFirstMove(chessBoard, move)) && Math.abs(yTo - yFrom) == 2){
                // ruch o dwa pola
                if(Color.WHITE == chessBoard.getPieceOnPosition(move.getFrom()).getColor()){
                    if( (yTo - yFrom == 2) && chessBoard.getPieceOnPosition(move.getSquare(0,1)) == null && chessBoard.getPieceOnPosition(move.getTo()) == null){
                        //
                    } else {
                        return false;
                    }
                } else {
                    if( (yTo - yFrom == -2) && chessBoard.getPieceOnPosition(move.getSquare(0,-1)) == null && chessBoard.getPieceOnPosition(move.getTo()) == null){
                        //
                    } else {
                        return false;
                    }
                }
            } else if(Math.abs(yTo - yFrom) == 1 ) {
                // ruch o jedno pole
                if(Color.WHITE == chessBoard.getPieceOnPosition(move.getFrom()).getColor()){
                    if( (yTo - yFrom == 1) && chessBoard.getPieceOnPosition(move.getTo()) == null){
                        //
                    } else {
                        return false;
                    }
                } else {
                    if( (yTo - yFrom == -1) && chessBoard.getPieceOnPosition(move.getTo()) == null){
                        //
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else if(Math.abs(xFrom - xTo) == 1 && Math.abs(yFrom - yTo) == 1) {
            // bicie
            if(Color.WHITE == chessBoard.getPieceOnPosition(move.getFrom()).getColor()
                    && chessBoard.getDoubleMovePosition(Color.BLACK).getX() == xTo
                    && chessBoard.getDoubleMovePosition(Color.BLACK).getY() == yTo - 1) {
                // bicie w przelocie białe
            } else if(Color.BLACK == chessBoard.getPieceOnPosition(move.getFrom()).getColor()
//                    && chessBoard.getDoubleMoveWhitePosition().getX() != -1
                    && chessBoard.getDoubleMovePosition(Color.WHITE).getX() == xTo
                    && chessBoard.getDoubleMovePosition(Color.WHITE).getY() == yTo + 1) {
                // bicie w przelocie czarne
            } else {
                if(Color.WHITE == chessBoard.getPieceOnPosition(move.getFrom()).getColor()){
                    if(chessBoard.getPieceOnPosition(move.getSquare(xTo - xFrom,1)) != null
                            && (yTo - yFrom) == 1
                            && Color.BLACK == chessBoard.getPieceOnPosition(move.getTo()).getColor() ){
                        // // bicie nie w przelocie białe
                    } else {
                        return false;
                    }
                } else {
                    if(chessBoard.getPieceOnPosition(move.getSquare(xTo - xFrom,-1)) != null
                            && (yTo - yFrom) == -1
                            && Color.WHITE == chessBoard.getPieceOnPosition(move.getTo()).getColor() ){
                        // // bicie nie w przelocie czarne
                    } else {
                        return false;
                    }
                }
            }
        } else {
            // nie można zrobić takiego ruchu
            return false;
        }

        return true;
    }

    private boolean wasFirstMove(Board chessBoard, Move move){
        Color color = chessBoard.getPieceOnPosition(move.getFrom()).getColor();
        int yFrom = move.getFrom().getY();
        if(color == Color.WHITE){
            if(yFrom == 1){
                return false;
            }
        } else {
            if(yFrom == 6){
                return false;
            }
        }
        return true;
    }
}
